" ▀█▀▄▀▄█▀▄█ █
"  █ ▀▄▀█▀▄▀▄█
"  simple, clean, elegant
"
" Page: https://toru.codeberg.page/
" Mastodon: @averagetiger89
" Repository: https://codeberg.org/toru
" Email: torupublic11@waifu.club

"       _                 
" __ __(_) _ __   _ _  __ 
" \ V /| || '  \ | '_|/ _|
"  \_/ |_||_|_|_||_|  \__|

let mapleader = " "                                                " map leader to Space
set number                                                        " add line numbers
set encoding=utf-8       " enconding
syntax on                                                         " syntax highlighting
set modeline             " enable vim modelines
set history=1000         " remember more commands and search history
set title                " change the terminal's title
set undolevels=1000      " use many muchos levels of undo
set autoindent
set termguicolors                                                 " terminal colors
set ignorecase                                                    " case insensitive 
set shortmess=I                                                   " disable neovim/vim start message
set noswapfile                                                    " disable creating swap file
set smarttab
set expandtab                                                     " using tab on insert-mode, it expands the space
set fillchars=eob:\                                               " hide the tildes 
set tabstop=2                                                     " using tab on insert-mode, it expands the space by two spaces
set mouse=v                                                       " middle-click paste with 
set hlsearch                                                      " highlight search 
set incsearch                                                     " incremental search
set mouse=a                                                       " enable mouse click
set clipboard=unnamedplus                                         " using system clipboard
set wildignore=*.swp,*.bak,*.pyc,*.class
set wildmode=longest,full
set sidescroll=1                                                  " smooth side scrolling
set conceallevel=2                                                " conceal marked text
set completeopt=menuone,noinsert,noselect                         " set the behavior of the completion menu 
set cmdheight=1                                                   " Height of the command line
set wildmenu                                                      " diplay command completion listing and choice menu
set wildignorecase                                                " ignore case command completion menu 
set timeoutlen=500                                                " timeout de ciertas funciones
set showmode                                                      " show mode (normal/visual/insert)
set showcmd
set laststatus=2                                                  " default statusline
set statusline=%-(%f%h%w%q%m%r%)%=%(%y\ %l/%L\ (%p%%)\ :\ %c%)    " settings for the default statusline
set noruler                                                       " show position of the line (bottom-right)
"set showtabline=2                                               " show tab even with only one file
"set cursorcolumn                                                " highlights the active line vertical
"set cursorline                                                  " highlights the active line horizontal
"set noerrorbells vb t_vb=	" remove all errors; 'set visualbell noeb' to revert
"set undofile             " set permanent undo (default `undodir = ~/.local/share/nvim/undo/` 

" Tab system
nnoremap <Tab> gt
nnoremap <silent> <C-t> :tabnew<CR>
nnoremap <silent> <C-c> :tabclose<CR>

" Move beetween windows
nnoremap <C-down> <C-W>j
nnoremap <C-up> <C-W>k
nnoremap <C-left> <C-W>h
nnoremap <C-right> <C-W>l
