# Simple, Clean, Elegant

These are a collection of my dotfiles, while the mayority of them, their default location is .config, others (like zshrc, cronjobs, etc) has their own independent location that i will explained below 
 --------------------------------------- 
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

## ¿what goes in $HOME? (~)
- .newsboat
- .bashrc
- .zshrc
- .xprofile
- .mpd
- .ncmpcpp

## general
everything else goes in ~/.config

## doas.conf
al tener el paquete de doas (en arch es opendoas), ese archivo va en /etc/ (/etc/doas.conf)

como el archivo lo tuve que copiar estando en propiedad del usuario (ya que realmente va en root /etc/), al migrar este archivo, se le tiene que hacer:  
doas chown -c root:root /etc/doas.conf
doas chmod -c 0400 /etc/doas.conf
